" nvim/init.vim
" Maintainer: Case Duckworth <cased123@gmail.com>
" Version: 2: Electric Boogaloo
" License: WTFPL
" vim:fdm=marker:foldlevel=0

source ~/.config/nvim/plugins.vim
 
" Functions {{{
function! SyntaxItem()
  return synIDattr(synID(line('.'),col('.'),1),'name')
endfunction
" }}}
" Settings {{{
set title
let $NVIM_TUI_ENABLE_CURSOR_SHAPE = 1
" set clipboard+=unnamedplus

set secure
set exrc

set autowriteall 
set hidden
set confirm

set backupdir=$XDG_DATA_HOME/nvim/backup/
set directory=$XDG_DATA_HOME/nvim/swap/
set viewdir=$XDG_DATA_HOME/nvim/view/
set viewoptions=folds,cursor,slash,unix

set updatecount=10 updatetime=500
set undofile undolevels=10000
set undodir=$XDG_DATA_HOME/nvim/undo/

set equalalways
set splitbelow splitright
set switchbuf=usetab

set number
set cursorline
set ruler 
set showcmd
set belloff=all
set wildmenu
set shortmess=aoOTIc
set statusline=%2n%{mode()}\ %q%f\ %y%m\ %a%=
set statusline+=%{SyntaxItem()}
set statusline+=\ %{fugitive#head(10)}\ %-10.(%l:%c%V%)%-4P

set gdefault
set inccommand=nosplit
set ignorecase smartcase
set wildignorecase

set linebreak breakindent showbreak=\\
set list listchars=tab:\,.,nbsp:~,extends:>,precedes:<
set sidescrolloff=1

set nrformats+=alpha

set tabstop=2 expandtab 
let &shiftwidth = &tabstop
let &softtabstop = &tabstop
set shiftround

set spelllang=en_us
set spellfile=$XDG_CONFIG_HOME/nvim/spell/en.utf8.add
set runtimepath+=/usr/share/vim/vimfiles

if executable('rg')
  set grepprg=rg\ --vimgrep\ --no-heading
  set grepformat=%f:%l:%c:%m,%f:%l%m,%f\ \ %l%m
elseif executable('ag')
  set grepprg=ag\ --vimgrep\ --nogroup\ --nocolor
  set grepformat=%f:%l:%c:%m,%f:%l%m,%f\ \ %l%m
endif

let g:is_bash = 1
"}}}
" Maps {{{
let mapleader="\<Space>"
let maplocalleader="\<Space>"

noremap ; :

nnoremap ' `
nnoremap ` '

nnoremap K i<CR><Esc>d^kg_lD
nnoremap <F1> K

nnoremap Q gqip
xnoremap Q gq

nnoremap <silent> <Leader>nr :set invnumber<CR>
nnoremap <silent> <Leader>/ :nohlsearch<CR>
nnoremap <Leader><Leader> za

nnoremap <Leader>so vip:sort<CR>
xnoremap <Leader>so :sort<CR>

nnoremap <C-j> <C-w>w
nnoremap <C-k> <C-w>W
nnoremap <expr> <C-c> (exists(':Sayonara') ? ':Sayonara<CR>' : '<C-w>c')
nnoremap <BS> <C-^>

nnoremap <expr> j (v:count > 1 ? 'm`' . v:count . 'j' : 'gj')
nnoremap <expr> k (v:count > 1 ? 'm`' . v:count . 'k' : 'gk')
nnoremap <expr> n (v:searchforward ? 'n' : 'N')
nnoremap <expr> N (v:searchforward ? 'N' : 'n')


"}}}
" Commands {{{

command! -nargs=+ -complete=file -bar Grep sil! gr! <args>|
      \ cw|redr!|let @/="<args>"|set hls
"}}}
" Autocommands {{{
augroup vimrc
  au!
  au BufWinEnter * silent! loadview | lcd %:p:h
  au BufWinLeave * silent! mkview

  au CursorHold,CursorHoldI * silent! update | noh
  au FocusLost * silent! wall | noh
augroup END

augroup textfiles
  au!
  au FileType pandoc,markdown setl spell
  au FileType pandoc,markdown setl spell
augroup END

augroup quickfixers
  au!
  au QuickFixCmdPost [^l]* cwindow
  au QuickFixCmdPost l*    lwindow
  au VimEnter        *     cwindow
augroup END

augroup dash
  au!
  fun! SetAshDash()
    if getline(1) =~? '#!.*d\?ash$'
      setlocal ft=sh
    endif
  endfunction
  au BufNewFile,BufRead * call SetAshDash()
augroup END
" }}}
