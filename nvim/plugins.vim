" Plugins and config

let g:plugins = resolve($HOME) . "/.local/vimplugins/"

" VIM-PLUG
call plug#begin(g:plugins)
Plug 'duckwork/vim-ft-c'
Plug 'Shougo/deoplete.nvim' " TODO a different completer?
Plug 'duckwork/vim-paper'
Plug 'junegunn/goyo.vim'
Plug 'justinmk/vim-dirvish'
Plug 'kovetskiy/sxhkd-vim'
Plug 'mhinz/vim-sayonara'
Plug 'nathangrigg/vim-beancount'
Plug 'nelstrom/vim-visual-star-search'
Plug 'rhysd/clever-f.vim'
Plug 'tommcdo/vim-exchange'
Plug 'tommcdo/vim-lion'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-eunuch'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-rsi'
Plug 'tpope/vim-surround'
call plug#end()

" PLUGIN SETTINGS
colorscheme paper

let g:ledger_bin = 'hledger'
let g:beancount_account_completion = 'default'
let g:beancount_detailed_first = 1
let g:beancount_separator_col = 60

let g:deoplete#enable_ignore_case = 1
let g:deoplete#enable_smart_case = 1

inoremap <silent> <expr> <TAB>
      \ pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <silent> <expr> <S-TAB>
      \ pumvisible() ? "\<C-p>" : "\<S-Tab>"

let g:goyo_width = 60
nnoremap <F11> :Goyo<CR>
function! s:goyo_enter()
  " Set up quitting stuff
  let b:quitting = 0
  let b:quitting_bang = 0
  autocmd QuitPre <buffer> let b:quitting = 1
  cabbrev <buffer> q! let b:quitting_bang = 1 <bar> q!
endfunction
function! s:goyo_leave()
  " Quit Vim if this is the only remaining buffer
  if b:quitting && len(filter(range(1, bufnr('$')), 'buflisted(v:val)')) == 1
    if b:quitting_bang
      qa!
    else
      qa
    endif
  endif
endfunction

augroup vimplugins
  au!
  au FileType dirvish call fugitive#detect(@%)
  au FileType beancount inoremap . .<Esc>:AlignCommodity<CR>a
  au! User GoyoEnter nested call <SID>goyo_enter()
  au! User GoyoLeave nested call <SID>goyo_leave()
augroup END
