#!/usr/bin/env python3

# Commands
# monitor=MONITOR_SEL
# desktop=DESKTOP_SEL
# node=NODE_SEL
# state=(tiled|pseudo_tiled|floating|fullscreen)
# layer=(below|normal|above)
# split_dir=(north|west|south|east)
# split_ratio=RATIO
# (hidden|sticky|private|locked|center|follow|manage|focus|border)=(on|off)

import os
import sys
import subprocess
import json

valid_locations = ['monitor', 'desktop', 'node']
desktop_rules = {
        'qutebrowser': 'web',
        'Firefox': 'web',
        'Chromium': 'web',
        'libreoffice': 'office'
        }
float_rules = { 
        'Sxiv': True, 
        'mpv': True, 
        'feh': True, 
        'quickbrowse': True }

class Window:
    '''Encodes a window with id, class, and instance'''
    def __init__(self, wid, wclass, winst):
        self.wid = wid
        self.wclass = wclass
        self.winst = winst

def transport(loc_type, loc_sel, follow=True):
    action = []
    if not loc_type in valid_locations:
        raise ValueError('Not a valid location')
    action.append('{}={}'.format(loc_type, loc_sel))
    if follow:
        action.append('follow=on')
    return action

def bspc_query(scope=['-m']):
    proc = subprocess.run(['bspc', 'query', '-T']+scope, stdout=subprocess.PIPE, check=True)
    return json.loads(proc.stdout)

def bspc_do(*args):
    subprocess.run(['bspc']+list(args), check=True)

def create_if_needed(desktop):
    for d in bspc_query()['desktops']:
        if d['name'] == desktop:
            break
    else:
        bspc_do('monitor', '-a', desktop)
    return desktop

def match_rule(window, rules):
    if window.wclass in rules:
        return rules[window.wclass]
    elif window.winst in rules:
        return rules[window.winst]

def printr(rdict):
    for (k, v) in rdict.items():
        print("{}={}".format(k, v))

def main(window):
    d = match_rule(window, desktop_rules)
    f = match_rule(window, float_rules)

    if d:
        create_if_needed(d)
        printr({'monitor': 'focused', 'desktop': d, 'follow': 'on'})

    if f:
        printr({'state': 'floating'})

if __name__ == '__main__':
    try:
        # os.popen('notify-send.sh "' + ' '.join(sys.argv[1:]) + '"')
        w = Window(sys.argv[1], sys.argv[2], sys.argv[3])
    except IndexError:
        raise IndexError("Need three arguments!")
    main(w)
    if sys.argv[4]:
        printr({'monitor': sys.argv[4]})
    if sys.argv[5]:
        printr({'desktop': sys.argv[5]})
    if sys.argv[6]:
        printr({'node': sys.argv[6]})
