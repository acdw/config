#!/bin/bash
#
# Rules
# Commands
# monitor=MONITOR_SEL
# desktop=DESKTOP_SEL
# node=NODE_SEL
# state=(tiled|pseudo_tiled|floating|fullscreen)
# layer=(below|normal|above)
# split_dir=(north|west|south|east)
# split_ratio=RATIO
# (hidden|sticky|private|locked|center|follow|manage|focus|border)=(on|off)

wid="$1";
class="$2";
instance="$3";

monitor="$4";
desktop="$5";
node="$6";

######################### DEBUG ########################
# notify-send.sh "$wid" "class=$class instance=$instance\nmon=$monitor desk=$desktop node=$node"

declare -A desktop_rules=(
  ['qutebrowser']='web'
  ['Firefox']='web'
  ['libreoffice']='office'
)

# DESKTOPS
create_if_needed() {
  desktop=$1
  if ! bspc query -T -m | \
     jq -er ".desktops[] | select(.name==\"$desktop\").name";
  then
    bspc monitor -a $desktop
    # if ! bspc query -D -d '^1.occupied'
    #   bspc desktop '^1' -r
    # fi
  fi
  echo monitor=focused
  echo desktop=$desktop
  echo follow=on
}

if [ ${desktop_rules[$class]+exists} ]; then
  desktop=${desktop_rules[$class]}
  create_if_needed $desktop
elif [ ${desktop_rules[$instance]+exists} ]; then
  desktop=${desktop_rules[$instance]}
  create_if_needed $desktop
fi

# FLOATING
case "$class" in
  Sxiv|mpv|feh)
    echo state=floating
    ;;
  wbar) echo layer=below ;;
esac
case "$instance" in
  quickbrowse)
    echo state=floating
    ;;
esac
