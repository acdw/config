#! /bin/sh
#
# Example panel for lemonbar

. $XDG_CONFIG_HOME/bspwm/lib/panel_colors.sh

num_mon=$(bspc query -M | wc -l)

while read -r line ; do
  case $line in
    S*) # system output
      name="${line#?}"
      case "${name}" in
        C*) clock="${name#?}" ;;
        B*) battery="${name#?}" ;;
        W*) wifi="${name#?}" ;;
        V*) volume="${name#?}" ;;
      esac
      sys="%{F$COLOR_SYS_FG}%{B$COLOR_SYS_BG}" 
      sys="${sys} ${wifi} ${battery} │"
      sys="${sys} %{A:galendae:}${clock}%{A}"
      sys="${sys} %{B-}%{F-}"
      ;;
    T*) # xtitle output
      name="${line#?}"
      title="%{+u}%{F$COLOR_TITLE_FG}%{B$COLOR_TITLE_BG}"
      title="${title}%{A:windowmenu -m:}%{A3:clientmenu -m:} ${name::80} "
      title="${title}%{A}%{A}%{B-}%{F-}%{-u}"
      ;;
    W*) # bspwm's state
      wm=""
      tg="%{F$COLOR_STATE_BG}"
      IFS=':'
      set -- ${line#?}
      while [ $# -gt 0 ] ; do
        item=$1
        name=${item#?}
        case $item in
          [mM]*)
            [ $num_mon -lt 2 ] && shift && continue
            title="%{F$COLOR_TITLE_FG}%{B$COLOR_TITLE_BG} $(xtitle) %{B-}%{F-}"
            case $item in
              m*) # monitor
                FG=$COLOR_MONITOR_FG
                BG=$COLOR_MONITOR_BG
                ;;
              M*) # focused monitor
                FG=$COLOR_FOCUSED_MONITOR_FG
                BG=$COLOR_FOCUSED_MONITOR_BG
                ;;
            esac
            wm="${wm}%{F${FG}}%{B${BG}}%{A:bspc monitor -f ${name}:} ${name} %{A}%{B-}%{F-}"
            ;;
          [fFoOuU]*)
            title="%{F$COLOR_TITLE_FG}%{B$COLOR_TITLE_BG} $(xtitle) %{B-}%{F-}"
            case $item in
              f*) # free desktop
                FG=$COLOR_FREE_FG
                BG=$COLOR_FREE_BG
                ;;
              F*) # focused free desktop
                FG=$COLOR_FOCUSED_FREE_FG
                BG=$COLOR_FOCUSED_FREE_BG
                ;;
              o*) # occupied desktop
                FG=$COLOR_OCCUPIED_FG
                BG=$COLOR_OCCUPIED_BG
                ;;
              O*) # focused occupied desktop
                FG=$COLOR_FOCUSED_OCCUPIED_FG
                BG=$COLOR_FOCUSED_OCCUPIED_BG
                ;;
              u*) # urgent desktop
                FG=$COLOR_URGENT_FG
                BG=$COLOR_URGENT_BG
                ;;
              U*) # focused urgent desktop
                FG=$COLOR_FOCUSED_URGENT_FG
                BG=$COLOR_FOCUSED_URGENT_BG
                ;;
            esac
            wm="${wm}%{F${FG}}%{B${BG}}%{A:bspc desktop -f '${name}':} ${name} %{A}%{B-}%{F-}"
            ;;
          L*) # layout
            case "${name}" in
              T) # tile
                layout=tile
                sym=+
                ;; 
              M) # monocle
                sym=_
                layout=monocle
                ;;
            esac
            wm="${wm}%{F$COLOR_STATE_FG}%{B$COLOR_STATE_BG}%{A:bspc desktop -l next:} [${sym}] %{A}%{B-}%{F-}"
            ;;
          T*) # state
            case "${name}" in
              T) # tiled
                sym=" "
                ;;
              P) # pseudotiled
                sym="⁼"
                ;;
              F) # float
                sym="⁻"
                ;;
              *)
                sym="${name}"
                ;;
            esac
            tg="${tg}%{A:bspc node -t ~floating:} ${sym} %{A}"
            ;;
          G*) # flags - S=sticky P=private L=locked 
            sym="$(echo "${name}" | sed 's/S/•/;s/P/†/;s/L/⌂/;')"
            tg="${tg} ${sym} "
            ;;
        esac
        shift
      done
      tg="${tg}%{F-}"
      ;;
  esac
  case $layout in
    monocle)
      format="%%{l}%s%%{c}%s%s%%{r}%s\n"
      # format="%%{l}%s%%{c}%%{U$COLOR_TITLE_FG}%%{+u}%s%%{-u}%s%%{U-}%%{r}%s\n"
      # if [ $(bspc query -N -n .local.window | wc -l) -ge 2 ]; then
      #   wbar on;
      # else
      #   wbar off;
      # fi
      ;;
    tile) 
      format="%%{l}%s%%{c}%s%s%%{r}%s\n"
      # wbar off; 
      ;;
  esac
  printf "${format}" "${wm}" "${title}" "${tg}" "${sys}"
done
