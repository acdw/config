#! /bin/sh

if xdo id -a "$PANEL_WM_NAME" > /dev/null ; then
	printf "%s\n" "The panel is already running." >&2
	exit 1
fi

trap 'trap - TERM; kill 0' INT TERM QUIT EXIT

[ -e "$PANEL_FIFO" ] && rm "$PANEL_FIFO"
mkfifo "$PANEL_FIFO"

bspc config top_padding $PANEL_HEIGHT
bspc subscribe report > "$PANEL_FIFO" &
xtitle -sf 'T%s\n' > "$PANEL_FIFO" &

paneldo() {
  printf "S$1%s\n" "$($2)" >"$PANEL_FIFO";
  while sleep $3; do
    printf "S$1%s\n" "$($2)" >"$PANEL_FIFO";
  done
}
paneldo B batstat 3 &
paneldo W wifistat 3 &

clock -sf 'SC%d %a %H:%M' > "$PANEL_FIFO" &

. $XDG_CONFIG_HOME/bspwm/lib/panel_colors.sh

panel_bar.sh < "$PANEL_FIFO" | \
  lemonbar -a 32 -n "$PANEL_WM_NAME" -g x$PANEL_HEIGHT -f "$PANEL_FONT" \
  -F "$COLOR_DEFAULT_FG" -B "$COLOR_DEFAULT_BG" \
  -u 1 -r 2 -R "$COLOR_DEFAULT_FG" -g -2-2 \
  | sh &

wid=$(xdo id -a "$PANEL_WM_NAME")
tries_left=20
while [ -z "$wid" -a "$tries_left" -gt 0 ] ; do
	sleep 0.05
	wid=$(xdo id -a "$PANEL_WM_NAME")
	tries_left=$((tries_left - 1))
done
[ -n "$wid" ] && xdo above -t "$(xdo id -N Bspwm -n root | sort | head -n 1)" "$wid"

wait
