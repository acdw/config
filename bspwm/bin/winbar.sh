#!/bin/bash

. $XDG_CONFIG_HOME/bspwm/lib/panel_colors.sh

LENGTH=30
fmt="%%{F%s}%%{B%s}%%{A:%s:}%%{A3:%s:} %-${LENGTH}s %%{A}%%{A}%%{F-}%%{B-}│";

getwins() {
  pfw="$1";
  bspc query -N -n .local.window |\
  while read win; do
    title="$(xtitle ${win})";
    if [[ "${pfw}" == "${win}" ]]; then
      wfg="$COLOR_TITLE_FG";
      wbg="$COLOR_TITLE_BG";
    else
      wfg="$COLOR_TITLE_BG";
      wbg="$COLOR_TITLE_FG";
    fi
    title="${title::$LENGTH}"
    printf "${fmt}" "${wfg}" "${wbg}" \
      "bspc node -f ${win}" "clientmenu -b -m ${win}" "${title}"
  done
}

getwins $(bspc query -N -n); printf '\n';

bspc subscribe node_focus |\
while read x monitor desktop window; do
  getwins "$window";
  printf '\n';
done
