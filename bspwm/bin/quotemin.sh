#!/bin/bash

W="$(bspc query -N -n)";
D="$(bspc query -D -d)";
Dn="$(bspc query -T -d | jq -r .name)";
Dns="$(bspc query -T -m | jq -r .desktops[].name)"

case "$1" in
  quote)
    grep -q "'$Dn" <<<$Dns || bspc monitor -a "'$Dn";
    bspc node $W --to-desktop "'$Dn";
    ;;
  unquote)
    if grep -q "'$Dn" <<<$Dns; then
      for i in $(bspc query -N -d "'$Dn" -n .window); do
        bspc node $i --to-desktop "$D" && break;
      done;
      bspc node $i -f;
      bspc node -s biggest;
      bspc desktop "'$Dn" -r;
    fi
    ;;
esac
