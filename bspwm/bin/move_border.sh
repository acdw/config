#!/bin/bash

# Move the window borders in a certain direction.

declare -A compl=(
  [north]=south
  [south]=north
  [east]=west
  [west]=east
)

(( $# == 2 )) || exit $# ;
dir=$1;
delta=$2;

if [[ ${compl[$dir]+exists} ]]; then
  # echo "bspc node @${dir} -r ${delta} || bspc node @${compl[$dir]} -r ${compldelta}"
  bspc node @${dir} -r ${delta} || bspc node @${compl[$dir]} -r ${delta}
else
  exit 127
fi
