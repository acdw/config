#!/usr/bin/env python3

import re
import json
import subprocess

class Query:
    def __init__(self, qclass, qinst, monitor='.*', desktop='.*', node='.*'):
        self.qclass = re.compile(qclass)
        self.qinst = re.compile(qinst)
        self.monitor = re.compile(monitor)
        self.desktop = re.compile(desktop)
        self.node = re.compile(node)

    def match(self, window, rule=all):
        return rule([re.match(self.qclass, window.wclass),
                     re.match(self.qinst, window.winst),
                     re.match(self.monitor, window.monitor),
                     re.match(self.desktop, window.desktop),
                     re.match(self.node, window.node)])

class Window:
    def __init__(self, wid, wclass, winst, monitor='focused', desktop='focused', node='focused'):
        self.wid = wid
        self.wclass = wclass
        self.winst = winst
        self.monitor = monitor
        self.desktop = desktop
        self.node = node

    def apply_rules(self, rules):
        for (k, v) in rules.items():
            print('{}={}'.format(k, v))

def bspc_query(scope='-m'):
    return json.loads(subprocess.getoutput('bspc query -T '+scope))

def bspc(*args):
    return subprocess.run(['bspc']+list(args), check=True)
