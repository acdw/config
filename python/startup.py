# PYTHONSTARTUP file

import atexit
import os
import readline

pythondatadir = \
        os.path.join(os.environ.get('XDG_DATA_HOME') or
        os.path.join(os.expanduser('~'), '.local', 'share'), 'python')
if not os.path.isdir(pythondatadir):
    os.makedirs(pythondatadir, 0o700)

histfile = os.path.join(pythondatadir, 'history')
try:
    readline.read_history_file(histfile)
except FileNotFoundError:
    pass
atexit.register(readline.write_history_file, histfile)
