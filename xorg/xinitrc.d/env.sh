# Environment variables for X

set -a

type st >/dev/null && TERMINAL=$(which st)
type qutebrowser >/dev/null && BROWSER=$(which qutebrowser)

XRESOURCES=$XDG_CONFIG_HOME/xorg/xresources
XMODMAP=$XDG_CONFIG_HOME/xorg/xmodmap

WALL="${XDG_CONFIG_HOME:-$HOME/.config}/colors/wall.current";
