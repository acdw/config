# Startup programs

if [ -f "$XRESOURCES" ]; then
    xrdb -I$XDG_CONFIG_HOME/colors "$XRESOURCES"
fi
if [ -f "$XMODMAP" ]; then
    xmodmap "$XMODMAP" &
fi

numlockx on &
urxvtd -q -o -f &
fava &
dunst &
# compton &
unclutter -b --exclude-root --timeout 3 --jitter 10 &
xsetroot -cursor_name left_ptr &
feh --no-fehbg --bg-tile $XDG_CONFIG_HOME/colors/bg.current &

# firefox &
