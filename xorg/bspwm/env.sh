set -a

# PANEL
PANEL_FIFO=/tmp/panel-fifo
PANEL_HEIGHT=16
PANEL_FONT="-*-terminus-medium-*-*-*-12-*-*-*-*-*-iso10646-*"
PANEL_WM_NAME=bspwm_panel

PATH="$XDG_CONFIG_HOME/bspwm/bin:$PATH"
