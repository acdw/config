# ENVIRONMENT VARIABLES
# vim:ft=sh

set -a

# XDG basedir specification
XDG_CACHE_HOME="$HOME/.cache"
XDG_CONFIG_DIRS="/etc/xdg"
XDG_CONFIG_HOME="$HOME/.config"
XDG_DATA_DIRS="/usr/local/share:/usr/share"
XDG_DATA_HOME="$HOME/.local/share"
mkdir -p "$XDG_CACHE_HOME" "$XDG_CONFIG_HOME" "$XDG_DATA_HOME" >/dev/null 2>&1 ;

# XDG compliance
GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-2.0/gtkrc"
GTK_RC_FILES="$XDG_CONFIG_HOME/gtk-1.0/gtkrc"
HISTFILE="$XDG_DATA_HOME/bash/history"
INPUTRC="$XDG_CONFIG_HOME/readline/inputrc"
LESSHISTFILE="$XDG_CACHE_HOME/less/history"
LESSKEY="$XDG_CONFIG_HOME/less/lesskey"
PASSWORD_STORE_DIR="$XDG_DATA_HOME/pass"
PYTHONSTARTUP="$XDG_CONFIG_HOME/python/startup.py"
TERMINFO="$XDG_DATA_HOME/terminfo"
TERMINFO_DIRS="$XDG_DATA_HOME/terminfo:/usr/share/terminfo"
VIMPAGER_RC="$XDG_CONFIG_HOME/nvim/pager.vim"
XCOMPOSEFILE="$XDG_CONFIG_HOME/xorg/xcompose"
XINITRC="$XDG_CONFIG_HOME/xorg/xinitrc"
mkdir -p "$(dirname $HISTFILE)" ;
mkdir -p "$PASSWORD_STORE_DIR" ;
mkdir -p "$TERMINFO" >/dev/null 2>&1 ;
mkdir -p "$TERMINFO_DIRS" >/dev/null 2>&1 ;
mkdir -p "$TMUX_TMPDIR" >/dev/null 2>&1 ;

if [ -d "$XDG_RUNTIME_DIR" ]; then
    NVIM_LISTEN_ADDRESS="$XDG_RUNTIME_DIR/nvim.socket"
    TMUX_TMPDIR="$XDG_RUNTIME_DIR/tmux"
    XAUTHORITY="$XDG_RUNTIME_DIR/xorg/xauthority"
else
    NVIM_LISTEN_ADDRESS=/tmp/nvim.socket
    TMUX_TMPDIR=/tmp/tmux
    XAUTHORITY=/tmp/Xauthority
fi
mkdir -p $TMUX_TMPDIR >/dev/null 2>&1 ;
mkdir -p $(dirname $XAUTHORITY) >/dev/null 2>&1 ;
touch $XAUTHORITY ;

# Miscellaneous directories
BUILDDIR="$HOME/.local/builds"
DLDIR="/tmp/dl"
LOCALBIN="$HOME/.local/bin"
TRASHDIR="$XDG_DATA_HOME/Trash"
mkdir -p "$BUILDDIR" >/dev/null 2>&1 ;
mkdir -p "$DLDIR" >/dev/null 2>&1 ;
mkdir -p "$LOCALBIN" >/dev/null 2>&1 ;
mkdir -p "$TRASHDIR" >/dev/null 2>&1 ;

# PROGRAMS
[ -d $LOCALBIN ] && PATH="$LOCALBIN:$PATH"
[ -d /usr/lib/surfraw ] && PATH="$PATH:/usr/lib/surfraw"
EDITOR="$(which nvim)";
if hash vimpager >/dev/null 2>&1; then
    PAGER=$(which vimpager)
else
    PAGER=$(which less)
fi
MANPAGER="pager"
TERMINAL="terminal"
TERMCMD="terminal"

# HISTORY
HISTIGNORE='&:ls:[bf]g:exit:history:reboot:shutdown'
HISTCONTROL=ignoreboth:erasedups
HISTFILESIZE=10000
HISTSIZE=5000
HISTTIMEFORMAT='%F %T '

# PROGRAM-SPECIFIC VARIABLES
## libreoffice ui
#SAL_USE_VCLPLUGIN=gen
## hledger file
# LEDGER_FILE="$HOME/doc/ledger/2017.ledger"
BEANCOUNT_FILE="$HOME/doc/ledger.beancount"
## rtv
RTV_URLVIEWER=urlscan
