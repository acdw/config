# Programs to start on bash login

# Set terminal colors
# if hash setcolors 2>/dev/null; then
#     setcolors "$XDG_CONFIG_HOME/colors/vte.current" 2>/dev/null;
# fi

# Set dircolors
eval "$(dircolors $XDG_CONFIG_HOME/colors/dircolors.current)"

# # GPG-SSH thing
# export GPG_TTY=$(tty)
# gpg-connect-agent updatestartuptty /bye >/dev/null 2>&1 &

# Turn on NumLock
#sleep 1 && setleds -D +num &
