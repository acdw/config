#!/bin/bash
# Developed under Archlinux

set -euo pipefail

echo "1. Move non-XDG compliant dotfiles into $HOME"
for f in ~/.config/_home/*; do
    tt="${f##*/}"; t="${HOME}/.${tt}";
    if [[ -f "${t}" || -d "${t}" ]]; then
        mv "${t}" "/tmp/${tt}" && echo "Moved '${tt}' to /tmp.";
    elif [[ -L "${t}" ]]; then
        rm "${t}"  && echo "Removed link '${t}.'";
    fi
    ln -s "${f}" "${t}" && echo "Linked '${f}' -> '${t}'.";
done

echo "2. Install vim-plug"
if hash nvim 2>/dev/null; then
    vimplug="$HOME/.config/nvim/autoload/plug.vim"
else
    vimplug="$HOME/.vim/autoload/plug.vim"
fi
echo "Installing vim-plug to $vimplug"
curl -fLo "$vimplug" --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim;

echo "3. Install packages"
echo "   (do this yourself, dummy)"
cat pacman/pkglist.txt
