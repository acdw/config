-- Paper theme
-- by Case Duckworth <acdw@zoho.com>

local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi
local xrdb = xresources.get_current_theme()

local gfs = require("gears.filesystem")
local theme = dofile(gfs.get_themes_dir().."xresources/theme.lua")

theme.font = "Go Mono 8"

return theme
