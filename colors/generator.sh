#!/bin/bash
# Generate colorscheme files from provided schema

# Prepare environment
tmpdir="/tmp/colorgenerator";
colordir="${XDG_CONFIG_HOME:-$HOME/.config}/colors"
mkdir -p "$tmpdir" "$colordir";
COLORFILE="${COLORFILE:-$colordir/scheme.current}"
verbose=0; force=0
schemes=( all )


# Helper functions
usage() {
  echo Documentation is forthcoming
  exit $1
}

log() { [ $verbose -eq 1 ] && echo "$@" >&2; }

mvold() {
  for file in "$@"; do
    if [[  -f "${file}" ]]; then
      mv "${file}" "${file}.old"
    fi
  done
}

prompt_replace() { # <file> <newfile>
  file="$1"; newf="$2";
  if [ $force -ne 1 ] && [ -f "${file}" ]; then
    read -p "Replace '$file'? [Y/n] " yn
    if [[ "$yn" =~ ^[Nn] ]]; then
      log "The generated file is at '$newf'."
      return 1;
    else
      mvold "$file";
    fi
  fi
  mv "$newf" "$file";
}

# Scheme functions
##################

_xorg() {
  schemefile="xorg.current";
  log "Generating X11 colorscheme..."
  cat <<EOF >"$tmpdir/$schemefile"
! Generated by $0 on $(date)
! special
#define myfg           $COLORFG
#define mybg           $COLORBG
#define mycc           $COLORCC
! black
#define mycolor0       $COLOR00
#define mycolor8       $COLOR08
! red
#define mycolor1       $COLOR01
#define mycolor9       $COLOR09
! green
#define mycolor2       $COLOR02
#define mycolor10      $COLOR10
! yellow
#define mycolor3       $COLOR03
#define mycolor11      $COLOR11
! blue
#define mycolor4       $COLOR04
#define mycolor12      $COLOR12
! magenta
#define mycolor5       $COLOR05
#define mycolor13      $COLOR13
! cyan
#define mycolor6       $COLOR06
#define mycolor14      $COLOR14
! white
#define mycolor7       $COLOR07
#define mycolor15      $COLOR15
EOF
  prompt_replace "$colordir/$schemefile" "$tmpdir/$schemefile"
}

_vte() {
  schemefile="vte.current"
  log "Generating vte colorscheme..."
  cat <<EOF >"$tmpdir/$schemefile"
0$COLOR00
1$COLOR01
2$COLOR02
3$COLOR03
4$COLOR04
5$COLOR05
6$COLOR06
7$COLOR07
8$COLOR08
9$COLOR09
10$COLOR10
11$COLOR11
12$COLOR12
13$COLOR13
14$COLOR14
15$COLOR15
EOF
  prompt_replace "$colordir/$schemefile" "$tmpdir/$schemefile"
}

_qutebrowser() {
  conffile="$XDG_CONFIG_HOME/qutebrowser/qutebrowser.conf"
  schemefile="qute.current"
  log "Generating qutebrowser colors..."
  cat <<EOF >"/tmp/$schemefile"
completion.fg = \${statusbar.fg}
completion.bg = \${statusbar.bg}
completion.alternate-bg = \${statusbar.bg}
completion.category.fg = $COLORBG
completion.category.bg = $COLORFG
completion.category.border.top = $COLORFG
completion.category.border.bottom = \${completion.category.border.top}
completion.item.selected.fg = \${completion.bg}
completion.item.selected.bg = $COLOR11
completion.item.selected.border.top = \${completion.item.selected.bg}
completion.item.selected.border.bottom = \${completion.item.selected.border.top}
completion.match.fg = $COLOR04
completion.scrollbar.fg = \${completion.fg}
completion.scrollbar.bg = \${completion.bg}
statusbar.fg = $COLORFG
statusbar.bg = $COLORBG
statusbar.fg.insert = \${statusbar.bg}
statusbar.bg.insert = $COLORBG
statusbar.fg.command = \${statusbar.fg}
statusbar.bg.command = \${statusbar.bg}
statusbar.fg.caret = $COLOR05
statusbar.bg.caret = \${statusbar.bg}
statusbar.fg.caret-selection = $COLOR13
statusbar.bg.caret-selection = \${statusbar.bg}
statusbar.progress.bg = \${statusbar.fg}
statusbar.url.fg = \${statusbar.fg}
statusbar.url.fg.success = \${statusbar.fg}
statusbar.url.fg.success.https = \${statusbar.fg}
statusbar.url.fg.error = $COLOR01
statusbar.url.fg.warn = $COLOR03
statusbar.url.fg.hover = $COLOR05
tabs.fg.odd = \${statusbar.bg}
tabs.bg.odd = \${statusbar.fg}
tabs.fg.even = \${tabs.fg.odd}
tabs.bg.even = \${tabs.bg.odd}
tabs.fg.selected.odd = \${tabs.bg.odd}
tabs.bg.selected.odd = \${tabs.fg.odd}
tabs.fg.selected.even = \${tabs.fg.selected.odd}
tabs.bg.selected.even = \${tabs.bg.selected.odd}
tabs.bg.bar = \${statusbar.bg}
tabs.indicator.start = $COLOR03
tabs.indicator.stop = $COLOR05
tabs.indicator.error = $COLOR01
tabs.indicator.system = rgb
hints.fg = \${statusbar.fg}
hints.bg = \${statusbar.bg}
hints.fg.match = \${statusbar.url.fg.hover}
downloads.bg.bar = \${statusbar.bg}
downloads.fg.start = $COLOR15
downloads.bg.start = $COLORBG
downloads.fg.stop = \${downloads.fg.start}
downloads.bg.stop = $COLORFG
downloads.fg.system = rgb
downloads.bg.system = rgb
downloads.fg.error = white
downloads.bg.error = red
webpage.bg = \${statusbar.bg}
keyhint.fg = \${statusbar.fg}
keyhint.fg.suffix = $COLOR04
keyhint.bg = \${statusbar.bg}
messages.fg.error = $COLORFG
messages.bg.error = $COLOR01
messages.border.error = $COLOR01
messages.fg.warning = \${statusbar.fg}
messages.bg.warning = $COLOR03
messages.border.warning = $COLOR03
messages.fg.info = \${statusbar.fg}
messages.bg.info = \${statusbar.bg}
messages.border.info = $COLORFG
prompts.fg = \${statusbar.fg}
prompts.bg = \${statusbar.bg}
prompts.selected.bg = $COLOR02
EOF
  log "Generating complete qutebrowser config with new colors...";
  expr="";
  while read line; do
    var="${line% = *}";
    val="${line#* = }";
    expr="${expr}s/${var} =.*/${var} = ${val}/;"
  done < /tmp/$schemefile
  sed "${expr}" < $conffile >$tmpdir/qutebrowser.conf
  prompt_replace "$conffile" "$tmpdir/qutebrowser.conf"
}

_homepage() {
  cat <<EOF >$XDG_CONFIG_HOME/qutebrowser/start.html
<!DOCTYPE html>
<html>
<head> <title>Begin</title>
<style>
body {
    background-color: $COLORBG;
    color: $COLORFG;
    max-width: 39em;
    margin: auto;
    font-family: "Roboto";
    font-size: 16px;
}
h1 { font-size: 16px; margin: 2em auto 2em auto; }
a:link { color: $COLOR03; }
a:visited { color: $COLOR05; }
a:hover { color: $COLOR07; background-color: $COLOR05; }
a:active { color: $COLOR07; background-color: $COLOR05; }
.center { margin: auto; }
</style> </head>
<body>
    <h1> Begin. </h1>
    <ul class="center">
        <li> <a href="https://gmail.com"> Gmail </a> </li>
        <li> <a href="https://facebook.com"> Facebook </a> </li>
        <li> <a href="https://news.ycombinator.com"> Hacker News </a> </li>
        <li> <a href="https://github.com"> Github </a> </li>
    </ul>
</body>
</html>
EOF
}

# _dunst() {
#   conffile="$XDG_CONFIG_HOME/dunst/dunstrc"
#   schemefile="dunst.current"
#   log "Generating dunst colors..."
#   cat <<EOF >"/tmp/$schemefile"
# [frame]
#     width = 2
#     color = "$COLORFG"

# [urgency_low]
#     # IMPORTANT: colors have to be defined in quotation marks.
#     # Otherwise the "#" and following would be interpreted as a comment.
#     background = "$COLORBG"
#     foreground = "$COLORFG"
#     timeout = 4

# [urgency_normal]
#     background = "$COLORFG"
#     foreground = "$COLORBG"
#     timeout = 8

# [urgency_critical]
#     background = "$COLOR09"
#     foreground = "$COLOR15"
#     timeout = 0
# EOF
#   log "Generating complete dunst config with new colors...";
#   expr="";
#   while read line; do
#     var="${line% = *}"
#     val="${line#* = }"
#     expr="${expr}s/${var} = .*/${var} = ${val}/;"
#   done < /tmp/$schemefile
#   sed "${expr}" < $conffile >$tmpdir/dunstrc
#   prompt_replace "$conffile" "$tmpdir/dunstrc"
# }

_bgtile() {
  schemefile="$colordir/bg.current"
  template="$colordir/bw.gif"
  convert "$template" -fill "$COLORBG" -opaque black \
    -fill "$COLORFG" -opaque white $tmpdir/bg.gif;
  prompt_replace "$schemefile" "$tmpdir/bg.gif";
}

_all() {
  _xorg;
  _vte;
  _qutebrowser;
  # _dunst;
  _bgtile;
  _homepage;
}

# MAIN ####################################################
while getopts 'vhfs:-' o; do
  case "${o}" in
    v) verbose=1 ;;
    f) force=1 ;;
    h) usage 0 ;;
    s) schemes=( $(tr ',' ' ' <<<$OPTARG) ) ;;
    -) break ;;
    *) echo "Unknown option '-$OPTARG'"; usage 1 ;;
  esac
done; shift $((OPTIND - 1));

# get colors
. "${1:-$COLORFILE}"

for s in ${schemes[@]}; do
  log "Generating scheme $s ..."
  _$s ;
done
rmdir $tmpdir 2>/dev/null;
feh --no-fehbg --bg-tile "$colordir/bg.current";
