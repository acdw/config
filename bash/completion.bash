# COMPLETION

completion_files=('/usr/share/git/completion/git-completion.bash')

for cf in "${completion_files[@]}"; do
    if [ -f "${cf}" ]; then
        source "${cf}"
    fi
done

hash pandoc 2>/dev/null && eval "$(pandoc --bash-completion)"
