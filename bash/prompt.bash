# PROMPT GOODNESS

if [ -f /usr/share/git/completion/git-prompt.sh ]; then
    source /usr/share/git/completion/git-prompt.sh
    _prompt_git_enable=true
elif [ -f /usr/share/git/git-prompt.sh ]; then
    source /usr/share/git/git-prompt.sh
    _prompt_git_enable=true
fi

if [ $_prompt_git_enable ]; then
    export GIT_PS1_SHOWCOLORHINTS=1
    export GIT_PS1_SHOWDIRTYSTATE=1
    export GIT_PS1_SHOWUNTRACKEDFILES=1
    export GIT_PS1_SHOWUPSTREAM=auto
fi

export PROMPT_COMMAND=_prompt_command
export PS2='\[\e[1;30m\]..  \[\e[m\]'

_git_status() {
  git rev-parse 2>/dev/null && git status --porcelain -b | \
    awk '
  /^## / {
          branch = $0;
          sub(/^## /, "", branch);
          sub(/\.\.\..*/, "", branch);

          if ($0 ~ /ahead /) {
            ahead = $0;
            sub(/.*ahead /,  "", ahead);
            sub(/\].*|, .*/, "", ahead);
          }

          if ($0 ~ /behind /) {
            behind = $0;
            sub(/.*behind /, "", behind);
            sub(/\].*|, .*/, "", behind);
          }

          next;
  }

  /^\?\? /  { untracked++; next; }
  /^U. /    { conflicts++; next; }
  /^.U /    { conflicts++; next; }
  /^DD /    { conflicts++; next; }
  /^AA /    { conflicts++; next; }
  /^.M /    { changed++;         }
  /^.D /    { changed++;         }
  /^[^ ]. / { staged++;          }

  END {
        printf(" \033[0;37m%s\033[m", branch);
        if (untracked + conflicts + changed + staged + behind + ahead == 0) {
          printf(" \033[1;32m%s\033[m", "ok");
        } else {
          if (untracked) 
            printf(" \033[1;3%dm%s\033[m%d", 3, "?", untracked);
          if (conflicts)
            printf(" \033[1;3%dm%s\033[m%d", 1, "!", conflicts);
          if (changed  )
            printf(" \033[1;3%dm%s\033[m%d", 2, "*", changed);
          if (staged   )
            printf(" \033[1;3%dm%s\033[m%d", 4, ".", staged);
          if (behind   )
            printf(" \033[1;3%dm%s\033[m%d", 5, "-", behind);
          if (ahead    )
            printf(" \033[1;3%dm%s\033[m%d", 6, "+", ahead);
         }
  }'
}
_fill() {
  for (( i=0; i<${1}; i++ )); do
    echo -n "${2}"
  done
}
_prompt_command() {
  EC=$?

  history -a

  pwd_escape="${PWD/${HOME}/"~"}";
  L_LEFT=${#pwd_escape}

  # update title
  if [[ -n "$DISPLAY" ]]; then
    if [[ -f "/tmp/title-set.$$" ]]; then
      titlestr="$(</tmp/title-set.$$)"
    else
      titlestr="$(basename $0)[$$] : ${pwd_escape}"
    fi
    printf '\e]0;%s\7\n' "$titlestr"
  fi

  # git status
  # [ $_prompt_git_enable ] && GS="$(__git_ps1 ': %s ')"
  GS="$(_git_status)"

  # battery level if discharging
  if grep -q Discharging /sys/class/power_supply/BAT0/status; then
      BATLEV="$(acpi | cut -d, -f2)-"
  else
      BATLEV=""
  fi

  # Prompt
  # prompt="\n- \[\e[1;30m\]${pwd_escape} "
  # prompt="${prompt}${GS}"
  # if [ $EC != 0 ]; then
  #   prompt="${prompt}\[\e[1;31m\]$EC "
  #   L_LEFT=$((L_LEFT + 1 + ${#EC}))
  # fi
  # prompt="${prompt}\[\e[m\]"
  # prompt="${prompt}$(_fill $((COLUMNS - L_LEFT - ${#GS} - ${#BATLEV} - 3)) "-")"
  # prompt="${prompt}${BATLEV}"
  # prompt="${prompt}\n; \[\e[m\]"
  if [ $EC != 0 ]; then
    ec_color="\[\e[0;31m\]"
  else
    ec_color="\[\e[0;32m\]"
  fi
  s1="│"
  s2="│"
  prompt="${ec_color}${s1}\[\e[m\] ${pwd_escape} ${GS}"
  prompt="${prompt}\n"
  prompt="${prompt}${ec_color}${s2} \[\e[m\]"

  PS1="${prompt}"
}
