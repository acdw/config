# ALIASES

## COMMAND ALIASES
# alias pacman="bb-wrapper --build-dir $BUILDDIR"
# alias aur="bb-wrapper --aur --build-dir $BUILDDIR"
alias o="rifle"
alias v="$PAGER"
alias e="$EDITOR"
alias notify-send="notify-send.sh"

## OPTION ALIASES
alias mkdir="mkdir -p"
alias ls="ls -FH --color=auto"
alias la="ls -a"
alias ll="ls -Al"
alias lines="wc -l"
alias rtv="BROWSER=surf rtv" # there's no option to set the browser

# FAT FINGER ALIASES
alias exti="exit"
alias sl="ls"
alias shutodwn="systemctl poweroff"

#### XDG Basedir spec compliance
alias mutt="mutt -F $XDG_CONFIG_HOME/mutt/muttrc"
alias offlineimap="offlineimap -c $XDG_CONFIG_HOME/offlineimap/config"
alias tmux="tmux -f $XDG_CONFIG_HOME/tmux/config"
alias startx="startx $XDG_CONFIG_HOME/xorg/xinitrc"

## other stuff
alias shsh='HISTFILE=/dev/null bash --norc'
