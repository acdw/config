# FUNCTIONS

# a file-browser-betterizer
if [[ -f /usr/share/doc/ranger/examples/bash_automatic_cd.sh ]]; then
    source /usr/share/doc/ranger/examples/bash_automatic_cd.sh
    f() {
    if [ -z "$RANGER_LEVEL" ]; then
        ranger-cd $@
    else
        exit
    fi
    }
fi

cleanhist() { # clean the history file of duplicates
  [[ -z "${HISTFILE}" ]] && export HISTFILE=$HOME/.bash_history;
  touch $HISTFILE;
  nl "$HISTFILE" |\
    sort -k2 | uniq -f1 | sort -n |\
    cut -f2 >/tmp/${USER}-history.bash;
  mv /tmp/${USER}-history.bash "$HISTFILE";
}

up() { # go up a few dirs
    local x='';
    for i in $(seq ${1:-1}); do
        x="$x../";
    done
    d $x;
}

edit() {
  local editor="${EDITOR:-/usr/bin/vi}"
  for arg in $@; do
    if ![[ -f "${arg}" ]]; then
      continue;
    elif ![[ -w "${arg}" ]]; then
      editor=/usr/bin/sudoedit;
    fi
  done
  $editor $@
}

open() {
  local opener="${OPENER:-/usr/bin/rifle}"
  for arg in $@; do
    if ! [[ -f "${arg}" ]]; then
      continue;
    elif ! [[ -r "${arg}" ]]; then
      opener="sudo $opener";
    fi
  done
  $opener $@
}
