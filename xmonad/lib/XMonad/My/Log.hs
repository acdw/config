module XMonad.My.Log 
    ( panel
    , logHook
    ) where

import GHC.IO.Handle (Handle)
import XMonad hiding (logHook)

import XMonad.Actions.DynamicWorkspaceOrder (getSortByOrder)
import XMonad.Actions.GroupNavigation (historyHook)
import XMonad.Actions.CopyWindow (wsContainingCopies)

import XMonad.Hooks.ToggleFade (toggleFadeLogHook)
import XMonad.Hooks.DynamicLog

import XMonad.Util.Lemonbar
import XMonad.Util.Loggers
import XMonad.Util.Run (hPutStrLn)
import XMonad.Util.SpawnNamedPipe (getNamedPipe)

import XMonad.My.Theme

logHook :: X ()
logHook = do
    historyHook
    toggleFadeLogHook 0.8
    h <- getNamedPipe "xpanel"
    copies <- wsContainingCopies
    let checkCopies ws
            | ws `elem` copies
              = lbColor copiesFG copiesBG
              . lbClick 1 ("xmonadctl view-"++ws)
              . space $ ws
            | otherwise
              = normalOpaque
              . lbClick 1 ("xmonadctl view-"++ws)
              . space $ ws
    dynamicLogWithPP 
        $ panel { ppHidden = checkCopies
                , ppOutput = maybe (\_ -> return ()) hPutStrLn h
                }


panel :: PP
panel = def
    { ppCurrent -- current workspace
      = \ws -> 
        lbColor focusFG focusBG
        . lbClick 1 ("xmonadctl view-"++ws)
        . space $ ws
    , ppVisible -- visible workspace
      = \ws ->
        normalOpaque . lbOverline
        . lbClick 1 ("xmonadctl view-"++ws)
        . space $ ws
    , ppHidden -- hidden workspace with windows
      = \ws ->
        normalOpaque
        . lbClick 1 ("xmonadctl view-"++ws)
        . space $ ws
    , ppHiddenNoWindows -- hidden workspace w/o windows
      = const ""
    , ppUrgent -- workspace with urgent window
      = lbColor urgentFG urgentBG . space
    , ppSort -- how to sort workspaces
      = getSortByOrder
    , ppWsSep -- separator b/w workspaces
      = ""
    ----
    , ppTitle -- the current window's title
      = normalOpaque . shorten 50
    , ppLayout -- the current layout's name
      = normalOpaque
      . lbClick 1 "xmonadctl next-layout"
      . layoutNameConverter
    ----
    , ppSep -- separator b/w sections
      = normalOpaque " "
    , ppOrder -- the order of sections
      = \(ws:l:t:xs) ->
      [ lbAlignLeft ++ ws
      , l
      , t
      , lbAlignRight ++ concat xs
      ]
    -- , ppOutput -- where to output the formatted string
    --   = maybe (\_ -> return ()) hPutStrLn mh
    , ppExtras -- loggers and other goodies
      = loggers
    } where
        layoutNameConverter l
          = case l of
              "Tabs by Vertical"          -> "|v|"
              "Tabs by Horizontal"        -> "|h|"
              "Full"                      -> "|_|"
              _                           -> l

space = wrap " " " "

normalOpaque = lbColor "" normalBG

loggers :: [Logger]
loggers = reverse
    [ normalOpaqueL dateLog
    , normalOpaqueL batLog
    , normalOpaqueL volumeLog
    , normalOpaqueL wifiLog
    , normalOpaqueL mpdLog
    ] where
        dateLog = wrapL "" " " $ date "%I:%M%P %a %d"
        batLog = wrapL "" (sepClr sep') 
            $ logCmd "batstat"
        volumeLog
          = wrapL "" (sepClr sep')
          . lbClickL 3 "volstat display"
          . lbClickL 1 "volstat toggle"
          $ logCmd "volstat"
        wifiLog = wrapL " " (sepClr sep') 
            $ logCmd "wifistat"
        mpdLog 
          = wrapL "" (sepClr $ " "++sep)
          . lbClickL 1 "urxvtc -n ncmpcpp -e ncmpcpp"
          . shortenL 25
          $ logCmd "mpdstat"
        normalOpaqueL = lbColorL "" normalBG
        sepClr = lbColor "#aaaaaa" normalBG
        sep' = wrap " " " " sep

sep :: String
-- sep = "│"
sep = "¦"
-- sep = "§"
