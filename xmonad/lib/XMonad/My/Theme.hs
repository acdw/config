module XMonad.My.Theme 
    ( normalBG, focusBG, urgentBG
    , normalFG, focusFG, urgentFG
    , copiesFG, copiesBG
    , xfont, lemonFont
    , normalBorderColor, focusedBorderColor
    , decTheme
    , promptTheme
    , module XMonad.Util.XResources
    ) where

-- all the theming stuff for xmonad

import Data.List
import XMonad.Layout.Decoration
import qualified XMonad.Prompt as P
import XMonad.Util.XResources

normalBG, focusBG, urgentBG, copiesBG :: String
normalFG, focusFG, urgentFG, copiesFG :: String

normalBG = background xresources
focusBG = "#546e7a"
urgentBG = color1 xresources
copiesBG = color8 xresources

normalFG = foreground xresources
focusFG = normalBG
urgentFG = color15 xresources
copiesFG = normalBG

xfont :: String
xfont = "-*-terminus-medium-*-*-*-12-*-*-*-*-*-iso10646-*"

lemonFont :: String
-- lemonFont = "Fixedsys Excelsior 3.01"
--lemonFont = "Roboto Mono:size=8"
lemonFont = xfont

normalBorderColor, focusedBorderColor :: String
normalBorderColor = normalBG
focusedBorderColor = focusBG

decTheme :: Theme
decTheme = def
    { activeColor = focusBG
    , inactiveColor = normalBG
    , urgentColor = urgentBG
    , activeBorderColor = focusBG
    , inactiveBorderColor = normalBG
    , urgentBorderColor = urgentBG
    , activeTextColor = focusFG
    , inactiveTextColor = normalFG
    , urgentTextColor = urgentFG
    , fontName = xfont
    , decoHeight = 14
    }

promptTheme :: P.XPConfig
promptTheme = P.def
    { P.font = xfont
    , P.bgColor = normalBG
    , P.fgColor = normalFG
    , P.fgHLight = focusFG
    , P.bgHLight = focusBG
    , P.borderColor = normalBG
    , P.promptBorderWidth = 1
    , P.position = P.Top
    , P.alwaysHighlight = True
    , P.height = 20
    , P.maxComplRows = Nothing
    , P.historySize = 1000
    , P.searchPredicate = isInfixOf
    }
