module XMonad.My.Hooks
    ( handleEventHook
    , manageHook
    , startupHook
    ) where

-- import XMonad.Float.SimplestFloatDec
import Data.Monoid
import XMonad hiding (handleEventHook, manageHook, startupHook)
import XMonad.Hooks.EwmhDesktops (fullscreenEventHook)
import XMonad.Hooks.InsertPosition
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.PositionStoreHooks
import XMonad.Hooks.ServerMode
import XMonad.My.Commands
import XMonad.My.Helpers
import XMonad.My.Theme
import XMonad.Util.SpawnOnce

handleEventHook :: Event -> X All
handleEventHook = composeAll
    [ docksEventHook
    , serverModeEventHookCmd' commands
    , fullscreenEventHook
    , positionStoreEventHook
    ]

manageHook :: ManageHook
manageHook = composeAll
    [ manageDocks
    , positionStoreManageHook Nothing
    , composeOne
        [ isFullscreen -?> doFullFloat
        -- Force dialog windows and pop-ups to be floating.
        , stringProperty "WM_WINDOW_ROLE" =? "pop-up" -?> doCenterFloat
        , stringProperty "WM_WINDOW_ROLE" =? gtkFile -?> doCenterFloat
        , isDialog -?> doCenterFloat
        , className =? "Xmessage" -?> doCenterFloat
        , className =? "Virtualbox" -?> doCenterFloat
        , className =? "Firefox" <&&> appName =? "Toplevel" -?> doFloat
        , className =? "Sxiv" -?> doCenterFloat
        , transience -- Move transient windows to their parent.
        -- Don't steal master
        , className =? "URxvt" -?> tileBelow
        -- everything else
        , pure True -?> normalTile
        ]
    ]
  where
    gtkFile = "GtkFileChooserDialog"
    normalTile = insertPosition Above Newer
    tileBelow = insertPosition Below Newer

startupHook :: X ()
startupHook = do 
    setFullscreenSupported
    lemonbar
    spawnOnce "xmonad-ticker.sh"
