module XMonad.My.Layout where

import XMonad hiding (layoutHook, (|||))
import XMonad.Hooks.ManageDocks
import XMonad.Layout.Spacing
import XMonad.Layout.ResizableTile
import XMonad.Layout.MultiToggle
import XMonad.Layout.MultiToggle.Instances
import XMonad.Layout.MultiToggle.Extra
import XMonad.Layout.NoBorders
import XMonad.Layout.Renamed
import XMonad.Layout.LayoutCombinators
import XMonad.Layout.ResizableTabs

import XMonad.My.Theme

import XMonad.Layout.Groups

layoutHook 
  = avoidStruts
  . lessBorders OnlyFloat
  . mkToggle (single NBGFULL)
  $ tall ||| Mirror tall ||| Full
  where 
    tall = Tall nmaster delta ratio
  -- -- . renamed [CutWordsLeft 2] . spacingWithEdge gapw
  -- $ resizableTallTabs rstc -- ||| Mirror restall
  -- where
  --   rstc = def
  --           { vNMaster = nmaster
  --           , vFrac = ratio
  --           , vDelta = delta
  --           , hNMaster = nmaster
  --           , hFrac = ratio
  --           , hDelta = delta
  --           , tabsTheme = decTheme
  --           , gapWidth = gapw
  --           }
    nmaster = 1
    ratio = toRational ((sqrt 5 - 1) / 2) -- golden
    delta = 1/25
  --   gapw = 4
