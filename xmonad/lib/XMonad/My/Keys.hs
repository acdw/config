module XMonad.My.Keys
    ( keys
    , rawKeys
    , mouseBindings
    ) where

import System.Exit (exitSuccess)

import XMonad hiding (keys, mouseBindings)
import XMonad.Util.EZConfig

import qualified Data.Map as M
import qualified XMonad.StackSet as W

import XMonad.Actions.CopyWindow
import XMonad.Actions.CycleWS
import XMonad.Actions.DynamicWorkspaces
import XMonad.Actions.GroupNavigation 
    ( nextMatch
    , nextMatchOrDo
    , Direction(..)
    )

import qualified XMonad.Actions.DynamicWorkspaceOrder as DO
import qualified XMonad.Actions.FlexibleResize as Flex

import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ToggleFade

import XMonad.Layout.Groups.Helpers
import XMonad.Layout.MultiToggle (Toggle(..))
import XMonad.Layout.MultiToggle.Extra
import XMonad.Layout.MultiToggle.Instances
import XMonad.Layout.ResizableTabs

import XMonad.Prompt.Window
import XMonad.Prompt.XMonad
import XMonad.Prompt.RunOrRaise
import XMonad.Prompt.Pass

import XMonad.My.Commands
import XMonad.My.Helpers
import XMonad.My.Theme (promptTheme)

------------------------------------------------------------

keys :: XConfig Layout -> M.Map (KeyMask, KeySym) (X ())
keys c = mkKeymap c (rawKeys c)

mouseBindings :: XConfig Layout 
              -> M.Map (KeyMask, Button) (Window -> X ())
mouseBindings XConfig { XMonad.modMask = mm }
  = M.fromList
    [ ((mm, button1), \w -> focus w 
                         >> mouseMoveWindow w)
    , ((mm, button2), windows 
                      . (W.shiftMaster .) 
                      . W.focusWindow)
    , ((mm, button3), \w -> focus w 
                         >> Flex.mouseResizeWindow w)
    ]

rawKeys :: XConfig Layout -> [(String, X ())]
rawKeys c = concatMap ($ c) keymaps where
    keymaps
      = [ appKeys
        , clientKeys
        , fnKeys
        , layoutKeys
        , screenKeys
        , workspaceKeys
        , xmonadKeys
        ]

appKeys :: XConfig Layout -> [(String, X ())]
clientKeys :: XConfig Layout -> [(String, X ())]
fnKeys :: XConfig Layout -> [(String, X ())]
layoutKeys :: XConfig Layout -> [(String, X ())]
screenKeys :: XConfig Layout -> [(String, X ())]
workspaceKeys :: XConfig Layout -> [(String, X ())]
xmonadKeys :: XConfig Layout -> [(String, X ())]

appKeys c
  = [ ("M-;", runOrRaisePrompt pt)
    , ("M-S-;", spawn "dmenu_run")
    , ("M-/", windowPromptGoto pt)
    , ("M-C-/", windowPromptBringCopy pt)
    , ("M-p", passPrompt pt)
    , ("M-S-p", passGeneratePrompt pt)
    , ("M-C-p", passRemovePrompt pt)
    , ("M-<Return>", spawn $ terminal c)
    ] where pt = promptTheme

clientKeys _
  = [ ("M-q", kill1)
    , ("M-C-q", kill)
    , ("M--", withFocused toggleFloat)
    , ("M-=", withFocused toggleFade)
    , ("M-`", nextMatch Forward (return True))
    ]

fnKeys _
  = [ ("<XF86AudioLowerVolume>", logspawn "er soft")
    , ("<XF86AudioRaiseVolume>", logspawn "er loud")
    , ("<XF86AudioMute>", logspawn "er mute")
    , ("<XF86MonBrightnessUp>", logspawn "er bright")
    , ("<XF86MonBrightnessDown>", logspawn "er dim")
    , ("<XF86AudioPlay>", logspawn "mpc toggle")
    , ("<XF86AudioStop>", logspawn "mpc pause")
    , ("<XF86AudioNext>", logspawn "mpc next")
    , ("<XF86AudioPrev>", logspawn "mpc prev")
    ] where logspawn cmd = spawn cmd >> runLogHook

layoutKeys _
  = lkClient ++ lkMessage ++ lkWithAll

screenKeys _
  = do
    (key, screen) <- [ ("<KP_Divide", screenBy (-1))
                     , ("<KP_Multiply>", screenBy 1)
                     ] 

    (mask, action) <- [ ("", W.view)
                      , ("S-", W.shift)
                      ]

    return ( "M-" ++ mask ++ key
           , screen >>= screenWorkspace
             >>= flip whenJust (windows . action)
           )

workspaceKeys _
  = wskViewShiftCopy ++ wskNavigate
  ++ [ ("M-<Tab>", toggleWS) ]

xmonadKeys c
  = [ ("M-S-q", io exitSuccess)
    , ("M-S-r", restarter)
    , ("M-a", commands >>= \c -> xmonadPromptC c promptTheme)
    ] where
        restarter
          = spawn "xmonad --recompile && xmonad --restart && notify-send.sh 'XMonad restarted'"
            >> (setLayout $ layoutHook c)

--------------------------------------------------
-- HELPERS
lkClient :: [(String, X ())]
lkMessage :: [(String, X ())]
lkWithAll :: [(String, X ())]
wskNavigate :: [(String, X ())]
wskViewShiftCopy :: [(String, X ())]

lkClient
  = [ ("M-j", focusGroupDown)
    , ("M-k", focusGroupUp)
    , ("M-S-j", swapGroupDown)
    , ("M-S-k", swapGroupUp)
    , ("M1-<Tab>", focusDown)
    , ("M1-S-<Tab>", focusUp)
    ]

lkMessage
  = [ ("M-f", sendMessage $ Toggle NBGFULL)
    , ("M-<Space>", nextOuterLayout)
    , ("M-b", sendMessage ToggleStruts)

    , ("M-m", focusGroupMaster)
    , ("M-S-m", swapGroupMaster)
    , ("M-h", shrinkMasterGroups)
    , ("M-l", expandMasterGroups)
    , ("M-[", increaseNMasterGroups)
    , ("M-]", decreaseNMasterGroups)

    , ("M-o", mirrorExpandGroups)
    , ("M-i", mirrorShrinkGroups)
    , ("M-S-h", moveToGroupUp False)
    , ("M-S-l", moveToGroupDown False)
    , ("M-C-h", moveToNewGroupUp)
    , ("M-C-l", moveToNewGroupDown)
    ]

lkWithAll
  = [ ("M-S-`", toggleFloatAll)
    ]

wskNavigate
  = do
    (key, direction) <- [ (",", Prev)
                        , (".", Next)
                        ]

    (mask, action) <- [ ("", DO.moveToGreedy)
                      , ("S-", \d t -> DO.shiftTo d t
                                    >> DO.moveToGreedy d t)
                      , ("C-", DO.swapWith)
                      ]

    return ( "M-" ++ mask ++ key
           , action direction NonEmptyWS
           )

wskViewShiftCopy
  = do
    (key, ws) <- zip (map show [1..9]) [0..]

    (mask, action) <- [ ("", W.greedyView)
                      , ("S-", W.shift)
                      , ("C-", copy)
                      ]
                      
    return ( "M-" ++ mask ++ key
           , DO.withNthWorkspace action ws
           )
