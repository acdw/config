{-# LANGUAGE TypeSynonymInstances, DeriveDataTypeable, MultiParamTypeClasses #-}

module XMonad.Layout.MultiToggle.Extra 
    ( ExtraTransformers(..)
    ) where

import XMonad.Layout.MultiToggle

import XMonad
import XMonad.Layout.NoBorders
import XMonad.Layout.Gaps

data ExtraTransformers 
    = NBGFULL    -- ^ switch to Full with no borders and a little gap
    deriving (Read, Show, Eq, Typeable)

instance Transformer ExtraTransformers Window where
    transform NBGFULL x k = k (gaps [(U, 2)] $ noBorders Full) (const x)
