{-# LANGUAGE TypeFamilies #-}
module XMonad.Layout.ResizableTabs 
    ( ResizableTabsConfig(..)
    , resizableTallTabs
    , shrinkText
    , defaultTheme
    , def
    , increaseNMasterGroups
    , decreaseNMasterGroups
    , shrinkMasterGroups
    , expandMasterGroups
    , nextOuterLayout
    , mirrorShrinkGroups
    , mirrorExpandGroups
    )where

import XMonad hiding ((|||))
import XMonad.Layout.LayoutCombinators

import qualified XMonad.Layout.Groups as G
import XMonad.Layout.Groups.Helpers
import XMonad.Layout.ResizableTile
import XMonad.Layout.Tabbed
import XMonad.Layout.Renamed
import XMonad.Layout.Decoration
import XMonad.Layout.Simplest
import XMonad.Layout.Spacing

data ResizableTabsConfig s 
  = RTC
  { vNMaster :: Int
  , vFrac :: Rational
  , vDelta :: Rational
  , hNMaster :: Int
  , hFrac :: Rational
  , hDelta :: Rational
  , tabsShrinker :: s
  , tabsTheme :: Theme
  , gapWidth :: Int
  }

instance s ~ DefaultShrinker => Default (ResizableTabsConfig s) where
    def = RTC 1 0.5 (3/100) 1 0.5 (3/100) shrinkText def 0

resizableTallTabs c = _tab c $ G.group _tabs $ _vert c ||| _horiz c

_tab c l = renamed [CutWordsLeft 1] $ 
           addTabs (tabsShrinker c) (tabsTheme c) l

_tabs = rename "Tabs" 
        Simplest

_vert c = rename "Vertical"
        $ spacingWithEdge (gapWidth c)
        $ ResizableTall (vNMaster c) (vDelta c) (vFrac c) []

_horiz c = rename "Horizontal"
         $ spacingWithEdge (gapWidth c)
         $ Mirror $ ResizableTall (hNMaster c) (hDelta c) (hFrac c) []

rename s = renamed [Replace s]

-- | Increase the number of master groups by one
increaseNMasterGroups :: X ()
increaseNMasterGroups = sendMessage $ G.ToEnclosing $ SomeMessage $ IncMasterN 1

-- | Decrease the number of master groups by one
decreaseNMasterGroups :: X ()
decreaseNMasterGroups = sendMessage $ G.ToEnclosing $ SomeMessage $ IncMasterN (-1)

-- | Shrink the master area
shrinkMasterGroups :: X ()
shrinkMasterGroups = sendMessage $ G.ToEnclosing $ SomeMessage $ Shrink

-- | Expand the master area
expandMasterGroups :: X ()
expandMasterGroups = sendMessage $ G.ToEnclosing $ SomeMessage $ Expand

-- | 'MirrorShrink' the layout
mirrorShrinkGroups :: X ()
mirrorShrinkGroups = sendMessage $ G.ToEnclosing $ SomeMessage $ MirrorShrink

-- | 'MirrorExpand' the layout
mirrorExpandGroups :: X ()
mirrorExpandGroups = sendMessage $ G.ToEnclosing $ SomeMessage $ MirrorExpand

-- | Rotate the available outer layout algorithms
nextOuterLayout :: X ()
nextOuterLayout = sendMessage $ G.ToEnclosing $ SomeMessage $ NextLayout
