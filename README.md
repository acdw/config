# ~/.config

## Part I. XDG specification and modularity
### (or, why I stopped worrying and love the bomb)

I like a clean `$HOME`. The way I've done it before, with dotfiles
cluttering up the place like so many dust bunnies, is gross and weighs on my
mind. I like the XDG spec for separating concerns (why yes, my configuration
files _do_ go in `.config`, and caches in `.cache` (though I don't
understand the complexity of `.local/share` (and now I'm three levels deep
in a parentheses (parenthese?)))), and found out online that most things can
be moved there. So here we are. In my `$HOME/.config` folder.

## Part II. Making sure bash still works
### (or, nothing is perfect)

_AFAIK_, bash won't just read random-ass files strewn about the system. So I
still need a `~/.bash{_profile,rc,_logout}`. I've included the files that need
to be linked to ~ in the `_home/` directory here.  To move them where they
should go, just run this:

    for file in ~/.config/_home/*; do
        ln -s "$file" ~/.${file};
    done

I've packaged this function, with some improvements, in `bootstrap.sh`.  Run it
to get the files where they need to be.

## Part II.i. Keep track of some rooted options
### (or, it's hard to remember everything I've done)

**TODO**

There's also a `_root` folder in here, which has copies of various files in
`/etc`, `/var`, etc. whose arcane incantations are too hard to remember between
installs.  There's no _set it and forget it_ option with these, since they can
be pretty sensitive, but there is a README file in that folder too that tells
you where everything goes.

## Part III. Sundries
### (or, it's late and I don't want to write this anymore)

- There's also great stuff in `nvim` --- I tried for a modular
  configuration. Hopefully it's not the **worst**.
- `cower` is an AUR helper, if you didn't know.
- `ranger` is a file manager, if you didn't know.

(NO LINKS FOR YOU!)

## Appendix A. Dependencies

(TODO).

## Appendix B. Annotated file tree

*Generated with `tree`.  Bash-style comments are at the top of each directory
that needs them.*

```bash
.
├── bash                 # Trusty shell
│   ├── alias.bash
│   ├── completion.bash  # Mostly sourcing others' functions for now...
│   ├── functions.bash
│   ├── login            # Executed by ~/.bash_profile
│   │   ├── dirs.bash
│   │   └── env.bash
│   ├── prompt.bash
│   ├── readline.bash    # Easier than using ~/.inputrc
│   └── settings.bash    # All the set -o stuff
├── beets                # Music organizer
│   └── config.yaml
├── bootstrap.sh
├── compton.conf         # X compositor
├── cower                # AUR helper
│   └── config
├── fontconfig           # various fontconfig files and stuff ...
├── _home                # Stuff that belongs in ~; see Section II
│   ├── bash_logout
│   ├── bash_profile
│   ├── bashrc
│   ├── gitconfig
│   ├── mutt
│   │   └── muttrc
│   └── xinitrc
├── htop
│   └── htoprc
├── ncmpcpp              # Music player
│   ├── bindings
│   └── config
├── nvim                 # Vim fork
│   ├── autoload         # My attempt at a modular config
│   │   ├── platform.vim # Platform-specific options
│   │   ├── plug.vim     # https://github.com/junegunn/vim-plug
│   │   ├── status.vim   # Statusbar functions
│   │   ├── surge.vim    # Plugins and settings
│   │   └── util.vim     # Utility functions
│   ├── init.vim
│   ├── plug
│   │   └── # all my plugins can be found in nvim/autoload/surge.vim
│   └── spel
│       └── en_us.utf-8.add
├── pacman
│   └── makepkg.conf
├── qutebrowser          # Web browser
│   ├── bookmarks
│   │   └── urls
│   ├── keys.conf
│   ├── quickmarks
│   └── qutebrowser.conf
├── ranger               # File browser
│   ├── colorschemes
│   │   ├── __init__.py
│   │   └── thatoldlook.py
│   ├── commands.py
│   ├── commands_sample.py
│   ├── rc.conf
│   ├── rifle.conf
│   ├── scope.sh
│   └── tagged
├── README.md
├── _root                # Root-level config stuff
├── surf                 # Web browser (insecure currently)
│   ├── bookmarks
│   ├── script.js
│   ├── simplyread.js
│   └── styles
│       ├── default.css
│       ├── facebook.com.css
│       └── github.com.css
├── tmux
│   └── config
├── vimpagerrc
├── vis
│   └── visrc.lua
└── xorg                 # These are all sourced by ~/.xinitrc
    ├── dwm
    │   └── status.sh
    ├── infinality-settings.sh
    ├── run_in_background.sh
    ├── wallpaper.sh
    └── xmodmap
```
